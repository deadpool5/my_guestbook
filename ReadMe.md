# How to start the project

- you should have docker installed locally.
- then run
  - ```docker-compose up -d```
- then run 
  - ```symfony server:start -d```
- Optional:- you have go through the blackfire to set it up in local
- To access the database use
  - ```docker-compose exec database psql app symfony```
- To Insert admin user with password (this during the chapter [Securing the Admin Backend](https://symfony.com/doc/current/the-fast-track/en/15-security.html))
  - ```INSERT INTO admin (id, username, roles, password) VALUES (nextval('admin_id_seq'), 'admin', '["ROLE_ADMIN"]', '$2y$13$gmScHr1DWQTGdFLoFdcUwORWXKyKlc2vWaOBjQL.AgdRdAs.LJPfy');```
  - default usernmae and password for ```/admin``` (admin, admin)
- in chapter [preventing spam with and api](https://symfony.com/doc/current/the-fast-track/en/16-spam.html)
  - you need to add ```AKISMET_KEY``` in ```.env.local``` file